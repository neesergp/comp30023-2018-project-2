#ifndef CSV_HANDLER
#define CSV_HANDLER

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define EXPECTED_LINE_LENGTH 1024
typedef struct NODE node;

//Node that stores the url and filename specified in the csv file
struct NODE
{
	char* url;
	char* filename;
	int valid;
	node* next;
};
//stores the start of the linked list
typedef struct
{
    node* start;
} head;
//loads the data into the linked list
head *load_csv(const char *filename);
//inserts data into the next node or start node
node *insert(node* nd, char* url, char* filename, int valid);
//cleans up the list
void delete_list(head* hd);
//dumps the content of the linked list into a output file
void output_csv(char* filepath, head* hd);



#endif
