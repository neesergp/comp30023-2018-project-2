### Author: Neeserg Parajuli
### Student num: 582811


## Program:

This program gets a path to a csv file, that contains (path to a certificate, url) and 
makes sure the certificate is still valid for that URL.


## Instructions:

1) type in `make clean` then `make` in the command prompt

2) type in `./certcheck [path to csv]` (make sure path to certificate stored in the csv is relative to the 
directory of the certcheck program)

3) check the output file.


## Files:

**certcheck.c**  is the main file that controls that stands as proxy between csv_handler and validater

**validater.c** validates a certificate

**csv_handler** reads csv data to memory and writes a csv file


:sweat: :exclamation: :fire: :+1: