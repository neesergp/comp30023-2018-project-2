
#ifndef HEADER
#define HEADER


#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>



X509 *load_cert(char * filename);
int check_san_valid(X509* cert, char* URL, int in_host);
int check_if_url_valid(char* URL, char* domain);
int check_name_valid(X509* cert,  char* URL);
int check_time(X509* cert);
int check_public_key(X509 *cert);
int check_constraint(X509 *cert);
int check_ext_key(X509 *cert);
int validate_certificate(char* filename, char* URL);


#endif