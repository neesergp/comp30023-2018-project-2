/**
    Example certifcate code
    gcc -o certexample certexample.c -lssl -lcrypto
*/


#include "headers.h"
#include "csv_handler.h"

int main(int argc, char const *argv[])
{
 

//Check if enough arguements was provided from the command line.
	if (argc < 2)
	{
		printf("Not enough input parameter");
		return 1;
	}


 // retrieve a linked list of url and its correponding certificate
    head* new = load_csv(argv[1]);

    node* nd = new->start;
//loop through and check for the validity of each certificate
    while(nd !=NULL){
    	nd-> valid = validate_certificate(nd->filename, nd->url);
    	nd = nd->next;
    }
//output the linked list to a output file
    output_csv("output.csv" ,new);
//cleanup
    delete_list(new);

    return 0;

}








