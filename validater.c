#include "headers.h"

int validate_certificate(char* filename, char* URL){
// loadf certificate from file
     X509* cert = load_cert(filename);

//check if url is the common name of the certificate 
     int in_host = check_name_valid(cert,  URL);

//check the if the url is in the SAN list given that its not already the common name
     if (!check_san_valid(cert, URL, in_host))
     {
        //printf("SAN \n");
        X509_free(cert);
        return 0;
     }
//check the expiry date
     if (!check_time(cert))
     {
        //printf("time\n");
        X509_free(cert);
        return 0;
     }
//check if the RSA key is 2048 bits
     if (!check_public_key(cert))
     {
        //printf("PKEY\n");
        X509_free(cert);
        return 0;
     }
//check if CA:FALSE

     if (!check_constraint(cert))
     {
        //printf("CONSTRAINTS\n");
        X509_free(cert);
        return 0;
     }
//check if the usage TLS is webserver authentication
     if (!check_ext_key(cert))
     {
        //printf("ext key\n");
        X509_free(cert);
        return 0;
     }
//clean up
     X509_free(cert);
     return 1;

}

X509 *load_cert(char * filename){
    BIO *certificate_bio = NULL;
    X509 *cert = NULL;

    //initialise openSSL
    OpenSSL_add_all_algorithms();
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();

    //create BIO object to read certificate
    certificate_bio = BIO_new(BIO_s_file());

    //Read certificate into BIO
    if (!(BIO_read_filename(certificate_bio, filename)))
    {
        fprintf(stderr, "Error in reading cert BIO filename");
        return NULL;
    }
    //Retrieve the certificate from the bio
    if (!(cert = PEM_read_bio_X509(certificate_bio, NULL, 0, NULL)))
    {
        fprintf(stderr, "Error in loading certificate");
        return NULL;
    }

   //clean up
    BIO_free_all(certificate_bio);
    return cert;
}





int check_name_valid(X509* cert,  char* URL){
    //retrive the name from the certificate
    X509_NAME *cert_issuer = X509_get_subject_name(cert);
    char subj_cn[256] = "Issuer CN NOT FOUND";
    //retrieve nthe name as string
    X509_NAME_get_text_by_NID(cert_issuer, NID_commonName, subj_cn, 256);
   //check if url is same as common name
       if (check_if_url_valid(URL, subj_cn))
    {
      return 1;
    }

    return 0;
}





int check_san_valid(X509* cert,  char* URL, int in_host){
    //dont proceed if the name is the common name
    if (in_host){
      return 1;
    }
    //get SAN from the extension
    X509_EXTENSION *ex = X509_get_ext(cert, X509_get_ext_by_NID(cert, NID_subject_alt_name, -1));
    if (ex == NULL)
    {
        return 0;
    }
   // load extension info on the bio and the retrieve extension information
    BUF_MEM *bptr = NULL;
    char *buf = NULL;

    BIO *bio = BIO_new(BIO_s_mem());
    if (!X509V3_EXT_print(bio, ex, 0, 0))
    {
        fprintf(stderr, "Error in reading extensions");
    }
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bptr);

    //bptr->data is not NULL terminated - add null character
    buf = (char *)malloc((bptr->length + 1) * sizeof(char));
    memcpy(buf, bptr->data, bptr->length);
    buf[bptr->length] = '\0';
    //Can print or parse value
    char *tok = strtok(buf, ", ");
    //check if the url matches any SANs listed in the extension
    while (tok !=NULL){
    if (check_if_url_valid(URL, tok+4)){
      BIO_free_all(bio);
      free(buf);
      return 1;
    }
    tok = strtok(NULL, ", ");

  }

    BIO_free_all(bio);
    free(buf);

    return 0;
}


int check_if_url_valid(char* URL, char* domain){
if (domain[0] == '*'){
  char* domain_1 = domain+1;
  char *sub_url = strstr(URL, domain_1);

  if (sub_url == NULL)
  {
    return 0;
  }

  else{
    return (strcmp(sub_url, domain_1) == 0);
  }

}
else
{
  return (strcmp(URL, domain) == 0);


}

return 0;


}








int check_time(X509* cert){
  //gets the expiry date
  ASN1_TIME *after = X509_get_notAfter(cert);
  if (after == NULL)
  {
   return 0;
  }

  int day;
  int sec;
  //function to that measures the difference between the two dates
  ASN1_TIME_diff(&day, &sec, NULL, after);

  if (day < 0 || sec <0)
  {
   return 0;
  }
  //gets the starting date
  ASN1_TIME *before = X509_get_notBefore(cert);
  if (before == NULL)
  {
   return 0;
  }

  
  ASN1_TIME_diff(&day, &sec, before, NULL);

  if (day < 0 || sec <0)
  {
   return 0;
  }

  return 1;


}






int check_public_key(X509 *cert){
  //gets the public key in EVP_pkey format
  EVP_PKEY *key = X509_get_pubkey(cert);
  RSA *rsa = NULL;
  //gets the rsa and checks if it is there
  if ((rsa = EVP_PKEY_get1_RSA(key)) == NULL)
  {
      return 0;
  }
  // checks if the rsa is 2048 bits
   if(8*RSA_size(rsa)==2048){
    return 1;
   }

  return 0;
}


int check_constraint(X509 *cert){
//gets the basic constraints stored in extension
 X509_EXTENSION *ex = X509_get_ext(cert, X509_get_ext_by_NID(cert, NID_basic_constraints, -1));
    if (ex == NULL)
    {
        return 0;
    }

    BUF_MEM *bptr = NULL;
    char *buf = NULL;

    BIO *bio = BIO_new(BIO_s_mem());
    if (!X509V3_EXT_print(bio, ex, 0, 0))
    {
        fprintf(stderr, "Error in reading extensions");
    }
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bptr);

    //bptr->data is not NULL terminated - add null character
    buf = (char *)malloc((bptr->length + 1) * sizeof(char));
    memcpy(buf, bptr->data, bptr->length);
    buf[bptr->length] = '\0';
    // checks if this constarins is in the list
    char* point = strstr(buf, "CA:FALSE");
    int cond = (point != NULL);
    BIO_free_all(bio);
    free(buf);

    return cond;
}


int check_ext_key(X509 *cert){
//gets the extended key usage from the extension
 X509_EXTENSION *ex = X509_get_ext(cert, X509_get_ext_by_NID(cert, NID_ext_key_usage, -1));
    if (ex == NULL)
    {
        return 0;
    }
    BUF_MEM *bptr = NULL;
    char *buf = NULL;

    BIO *bio = BIO_new(BIO_s_mem());
    if (!X509V3_EXT_print(bio, ex, 0, 0))
    {
        fprintf(stderr, "Error in reading extensions");
    }
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bptr);

    //bptr->data is not NULL terminated - add null character
    buf = (char *)malloc((bptr->length + 1) * sizeof(char));
    memcpy(buf, bptr->data, bptr->length);
    buf[bptr->length] = '\0';
    // checks if this usage is in the list
    char* point = strstr(buf, "TLS Web Server Authentication");
    int cond = (point != NULL);
    BIO_free_all(bio);
    free(buf);

    return cond;
}

